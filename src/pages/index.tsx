import React from "react";
import {Divider, Typography} from "@mui/material";
import {useTranslation} from "react-i18next";
import Head from "next/head";

import {useUser} from "src/store";
import PlayNowPanel from "src/components/home/PlayNowPanel";
import YourCollection from "src/components/home/YourCollection";
import LoginPanel from "src/components/home/LoginPanel";

const HomePage = () => {
    const {t} = useTranslation();
    const token = useUser(state => state.token);
    const isLoggedIn = !!token;

    return (
        <>
            <Head>
                <title>Draw With Us</title>
            </Head>
            <div className="grid grid-cols-[1fr_auto_1fr] h-[270px] justify-evenly bg-white rounded-xl p-2">
                <PlayNowPanel />
                <Divider orientation="vertical" flexItem variant="middle">
                    <Typography className="uppercase">{t("or")}</Typography>
                </Divider>
                {isLoggedIn ? <YourCollection /> : <LoginPanel />}
            </div>
        </>
    );
};

export default HomePage;
