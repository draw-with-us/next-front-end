import React, {ChangeEvent} from "react";
import {Button} from "@mui/material";
import MeetingRoomIcon from "@mui/icons-material/MeetingRoom";
import SportsEsportsIcon from "@mui/icons-material/SportsEsports";
import {useQueryClient} from "@tanstack/react-query";
import clsx from "clsx";
import {useTranslation} from "react-i18next";
import {NextPage} from "next";

import styles from "src/styles/Room.module.scss";
import RoomLayout from "src/core/layout/RoomLayout";
import {useRooms} from "src/api/services/RoomServices";
import SearchField from "src/core/SearchField";
import {useSocket} from "src/store";
import TopTooltip from "src/core/TopTooltip";
import useSearch from "src/core/hooks/useSearch";
import {
    RoomCard,
    RoomDefault,
    RoomProps,
    ShowModeSelector,
    useJoinRoom,
} from "src/components/list-room";

const ListRoomPage: NextPage = () => {
    const {t} = useTranslation();
    const [selectedRoom, setSelectedRoom] = React.useState<RoomProps>();
    const [defaultRooms] = React.useState(() => {
        return Array.from({length: 6}, () => RoomDefault());
    });
    const socket = useSocket();
    const {data} = useRooms();
    const queryClient = useQueryClient();
    const {joinRoom} = useJoinRoom();
    const combinedRooms = React.useMemo(() => {
        return [...(data ?? []), ...defaultRooms];
    }, [data, defaultRooms]);

    React.useEffect(() => {
        socket?.on("list-room:update", async () => {
            await queryClient.invalidateQueries(["rooms"]);
        });
        return () => {
            socket?.off("list-room:update");
        };
    }, [queryClient, socket]);

    const [filtered, debouncedSearch] = useSearch({
        data: combinedRooms,
        keys: ["id", "name", "collectionName"],
    });

    const onSearch = React.useCallback(
        (e: ChangeEvent<HTMLTextAreaElement>) => {
            const keyword = e.target.value;
            debouncedSearch(keyword);
        },
        [debouncedSearch]
    );
    const onRoomSelect = (room: RoomProps) => setSelectedRoom(room);
    const onJoinRoom = async () => {
        if (!selectedRoom) return;
        const {isPrivate, eid} = selectedRoom;
        await joinRoom({eid, isPrivate: !!isPrivate});
    };

    const isDisable = !selectedRoom;
    const tooltipText = isDisable
        ? t("list_room.play_tooltip.disable")
        : t("list_room.play_tooltip.enable");

    return (
        <RoomLayout
            title={t("list_room.title")}
            headerChildren={
                <SearchField
                    className="w-[130px]"
                    placeholder={t("list_room.search_place_holder")}
                    onChange={onSearch}
                />
            }
            endChildren={<ShowModeSelector />}
        >
            <div
                className={clsx(
                    styles.mainPanel,
                    "w-full max-w-full grid grid-cols-4 gap-2 px-2 scrollBar"
                )}
            >
                {(filtered?.length ? filtered : combinedRooms).map(room => (
                    <RoomCard
                        {...room}
                        key={room.eid}
                        selected={room.eid == selectedRoom?.eid}
                        onClick={() => onRoomSelect(room)}
                    />
                ))}
            </div>
            <div className="w-full flex justify-center mt-2 px-2">
                <Button
                    className="mr-2"
                    startIcon={<MeetingRoomIcon />}
                    variant="contained"
                    onClick={() => alert("/create")}
                >
                    {t("list_room.new_room")}
                </Button>
                <TopTooltip title={tooltipText}>
                    <span>
                        <Button
                            startIcon={<SportsEsportsIcon />}
                            variant="contained"
                            disabled={isDisable}
                            className="w-[135px]"
                            onClick={onJoinRoom}
                        >
                            {t("list_room.play")}
                        </Button>
                    </span>
                </TopTooltip>
            </div>
        </RoomLayout>
    );
};

export default ListRoomPage;
