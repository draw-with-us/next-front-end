import React from "react";
import {useQueryClient} from "@tanstack/react-query";

import GetPassword from "src/utils/PasswordDialog";
import {notifyError} from "src/utils/Notify";
import {useSocket} from "src/store";

type Params = {
    eid: string;
    isPrivate: boolean;
};

const useJoinRoom = () => {
    const socket = useSocket();
    const queryClient = useQueryClient();

    const joinRoom = React.useCallback(
        async ({eid, isPrivate}: Params) => {
            let password: string | undefined;

            if (isPrivate) {
                password = await GetPassword();
                if (!password) return;
            }
            socket?.emit(
                "room:join",
                {eid, password},
                async ({message, roomId, onMiddleGame}) => {
                    if (roomId)
                        alert("navigate to /room/" + roomId + onMiddleGame);
                    // return navigate(`/play/${roomId}`, {
                    //     state: {onMiddleGame},
                    // });
                    if (message) {
                        await queryClient.invalidateQueries(["rooms"]);
                        await notifyError(message as string);
                    }
                }
            );
        },
        [queryClient, socket]
    );

    return {joinRoom};
};

export default useJoinRoom;
