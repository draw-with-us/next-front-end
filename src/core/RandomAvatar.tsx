import React from "react";
import {Avatar, AvatarProps} from "@mui/material";

const StyledAvatar = ({className, ...others}: AvatarProps) => {
    return <Avatar className={`avatar ${className}`} {...others} />;
};

interface Props {
    size: number;
    value?: string;
}

const RandomAvatar = (props: Props & Omit<AvatarProps, "size">) => {
    const {value, size, ...others} = props;
    const defaultValue = "https://cdn.trinhdvt.tech/avatar.png";

    const [base64, setBase64] = React.useState("");
    React.useEffect(() => {
        import("jdenticon")
            .then(({toSvg}) => {
                const svgString = toSvg(value ?? defaultValue, size);
                const base64 = Buffer.from(svgString, "utf8").toString(
                    "base64"
                );
                setBase64(`data:image/svg+xml;base64,${base64}`);
            })
            .catch(() => ({}));
    }, [size, value]);

    return <StyledAvatar src={base64} alt="User's avatar" {...others} />;
};

export default RandomAvatar;
export {StyledAvatar};
