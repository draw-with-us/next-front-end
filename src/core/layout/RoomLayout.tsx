import {Grid} from "@mui/material";
import React from "react";
import Head from "next/head";

import styles from "src/styles/Room.module.scss";
import RoomHeader, {HeaderProps} from "src/core/layout/RoomHeader";

interface RoomLayoutProps extends HeaderProps {
    children: React.ReactNode;
}

const RoomLayout = (props: RoomLayoutProps) => {
    const {children, title, headerChildren, endChildren, ...others} = props;

    return (
        <>
            <Head>
                <title>{`${title} - Draw With Us`}</title>
            </Head>
            <Grid container className={styles.container} {...others}>
                <RoomHeader
                    title={title}
                    headerChildren={headerChildren}
                    endChildren={endChildren}
                />
                {children}
            </Grid>
        </>
    );
};

export default RoomLayout;
