import React from "react";
import Image from "next/image";
import {useRouter} from "next/router";

const AppHeader = () => {
    const router = useRouter();
    return (
        <div className="flex justify-center mb-4 max-h-[80px]">
            <Image
                src="/images/logo.svg"
                width="400px"
                height="100px"
                className="cursor-pointer"
                alt="Logo"
                onClick={() => router.replace("/")}
            />
        </div>
    );
};

export default AppHeader;
