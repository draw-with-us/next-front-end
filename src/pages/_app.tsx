import "src/styles/globals.css";
import type {AppProps} from "next/app";
import {StyledEngineProvider, ThemeProvider} from "@mui/material";
import {QueryClient, QueryClientProvider} from "@tanstack/react-query";
import {GoogleOAuthProvider} from "@react-oauth/google";

import {appTheme} from "src/core/AppTheme";
import SocketWrapper from "src/store/SocketStore";
import AppLayout from "src/core/layout/AppLayout";

const queryClient = new QueryClient({
    defaultOptions: {
        queries: {
            refetchOnWindowFocus: false,
        },
    },
});

const MyApp = ({Component, pageProps}: AppProps) => {
    // @ts-ignore
    const Layout = Component.Layout || AppLayout;

    return (
        <StyledEngineProvider injectFirst>
            <ThemeProvider theme={appTheme}>
                <QueryClientProvider client={queryClient}>
                    <GoogleOAuthProvider clientId="545452035521-leue10pl1h50n85g7r967bf43qjea24d.apps.googleusercontent.com">
                        <SocketWrapper>
                            <Layout>
                                {/*// @ts-ignore*/}
                                <Component {...pageProps} />
                            </Layout>
                        </SocketWrapper>
                    </GoogleOAuthProvider>
                </QueryClientProvider>
            </ThemeProvider>
        </StyledEngineProvider>
    );
};

export default MyApp;
